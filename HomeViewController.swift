//
//  HomeViewController.swift
//  EntityManagement
//
//  Created by Ashish on 17/05/21.
//

import UIKit

class HomeViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    // cell reuse id (cells that scroll out of view can be reused)
      let cellReuseIdentifier = "cell"
    @IBOutlet var tableView: UITableView!
    
    @IBOutlet weak var lnameLbl: UILabel!
    @IBOutlet weak var fnameLbl: UILabel!
    @IBOutlet weak var emailLbl: UILabel!
    @IBOutlet weak var telephoneLbl: UILabel!
    
    
    let Dict = NSDictionary()
    var ListArray = NSArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()

       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let semaphore = DispatchSemaphore (value: 0)


        var request = URLRequest(url: URL(string: "http://ec2-35-179-12-182.eu-west-2.compute.amazonaws.com/api/entities/list")!,timeoutInterval: Double.infinity)
        request.httpMethod = "GET"
        var mydata = Data()
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
          guard let data = data else {
            print(String(describing: error))
            semaphore.signal()
            return
          }
          print(String(data: data, encoding: .utf8)!)
          semaphore.signal()
            mydata = data
            
        }
        
        task.resume()
        semaphore.wait()
        
       
               tableView.delegate = self
               tableView.dataSource = self
        // Do any additional setup after loading the view.
        
        do {
            ListArray = try JSONSerialization.jsonObject(with: mydata, options:[]) as! NSArray
            print(ListArray)
            // print: 1,2,3,4,5,6,7,8,9
            tableView.reloadData()
        }catch{
            print("Error in Serialization")
        }
        
    }
    
    
    
    // number of rows in table view
      func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ListArray.count
      }
      
      // create a cell for each table view row
      func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
          
          // create a new cell if needed or reuse an old one
        let cell:UITableViewCell = (self.tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as UITableViewCell?)! 
          
        cell.textLabel?.text = ((ListArray[indexPath.row] as! NSDictionary).value(forKey: "fname")  as! String) + " " + ((ListArray[indexPath.row] as! NSDictionary).value(forKey: "lname")  as! String) 
            //ListArray[1]["fname"] ?? ""
      
          return cell
      }
      
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
           print("You tapped cell number \(indexPath.row).")
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "EditViewController") as! EditViewController
        vc.dict = (ListArray[indexPath.row] as! NSDictionary)
        self.navigationController?.pushViewController(vc, animated: true)
       }
    
    
    @IBAction func actionBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
