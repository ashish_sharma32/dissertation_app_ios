//
//  EditViewController.swift
//  EntityManagement
//
//  Created by Ashish on 17/05/21.
//

import UIKit

class EditViewController: UIViewController {

    @IBOutlet weak var fname: UITextField!
    @IBOutlet weak var lname: UITextField!
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var telephone: UITextField!
    
  public  var dict = NSDictionary()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //entities/update/{id}

        fname.text = (dict.value(forKey: "fname")  as! String)
        lname.text = (dict.value(forKey: "lname")  as! String)
        email.text = (dict.value(forKey: "email")  as! String)
        telephone.text = (dict.value(forKey: "telephone")  as! String)
        
        // Do any additional setup after loading the view.
    }
    
    
    
    @IBAction func updateAction(_ sender: Any) {
        
        let semaphore = DispatchSemaphore (value: 0)

        var request = URLRequest(url: URL(string: "http://ec2-35-179-12-182.eu-west-2.compute.amazonaws.com/api/entities/update/\((dict.value(forKey: "id") ?? ""))?fname=\(fname.text ?? "")&lname=\(lname.text ?? "")&email=\(email.text ?? "")&telephone=\(telephone.text ?? "")")!,timeoutInterval: Double.infinity)
        request.httpMethod = "POST"

        let task = URLSession.shared.dataTask(with: request) { data, response, error in
          guard let data = data else {
            print(String(describing: error))
            semaphore.signal()
            return
          }
          print(String(data: data, encoding: .utf8)!)
          semaphore.signal()
        }

        task.resume()
        semaphore.wait()
        
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
   
    @IBAction func deleteaction(_ sender: Any) {
        
        let semaphore = DispatchSemaphore (value: 0)

        var request = URLRequest(url: URL(string: "http://ec2-35-179-12-182.eu-west-2.compute.amazonaws.com/api/entities/delete/\((dict.value(forKey: "id") ?? ""))")!,timeoutInterval: Double.infinity)
        request.httpMethod = "GET"

        let task = URLSession.shared.dataTask(with: request) { data, response, error in
          guard let data = data else {
            print(String(describing: error))
            semaphore.signal()
            return
          }
          print(String(data: data, encoding: .utf8)!)
          semaphore.signal()
        }

        task.resume()
        semaphore.wait()
        
        self.navigationController?.popViewController(animated: true)
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
