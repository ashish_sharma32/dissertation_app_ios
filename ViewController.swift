//
//  ViewController.swift
//  EntityManagement
//
//  Created by Ashish on 17/05/21.
//

import UIKit

class ViewController: UIViewController {
    
    
    @IBOutlet weak var fnameTxt: UITextField!
    @IBOutlet weak var lnameTxt: UITextField!
    @IBOutlet weak var emailTxt: UITextField!
    @IBOutlet weak var telephoneTxt: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        
    }

    
    @IBAction func addUserAction(_ sender: Any) {
        
        let semaphore = DispatchSemaphore (value: 0)

        var request = URLRequest(url: URL(string: "http://ec2-35-179-12-182.eu-west-2.compute.amazonaws.com/api/entities/store?fname=\(fnameTxt.text ?? "")&lname=\(lnameTxt.text ?? "")&email=\(emailTxt.text ?? "")&telephone=\(telephoneTxt.text ?? "")")! ,timeoutInterval: Double.infinity)
        request.httpMethod = "POST"

        let task = URLSession.shared.dataTask(with: request) { data, response, error in
          guard let data = data else {
            print(String(describing: error))
            semaphore.signal()
            return
          }
          print(String(data: data, encoding: .utf8)!)
          semaphore.signal()
        }

        task.resume()
        semaphore.wait()
    }
    

}

